import { DataObject, EdmMapping } from '@themost/data';

@EdmMapping.entityType('ConsentField')
class ConsentField extends DataObject {
    constructor() {
        super();
    }
}

export {
    ConsentField
}