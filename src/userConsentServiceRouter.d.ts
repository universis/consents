import { ApplicationBase } from '@themost/common';

export declare function userConsentServiceRouter(app: ApplicationBase): Express.Router;