import { DataContext } from '@themost/data';
import { GenericConsentFieldDocument } from './GenericConsentFieldGroup';
export declare class UserConsentService {
    async getConsentsForCurrentUser(context: DataContext): Promise<GenericConsentFieldDocument>;
    async getConsentsForUser(context: DataContext, user: string): Promise<GenericConsentFieldDocument>;
    async getConsentGroupForCurrentUser(context: DataContext): Promise<GenericConsentFieldDocument>;
    async getConsentGroupForUser(context: DataContext, user: string, group: string): Promise<GenericConsentFieldDocument>;
    async setConsentsForCurrentUser(context: DataContext, consents: GenericConsentFieldDocument): Promise<number>;
}