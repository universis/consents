import { GenericConsentField } from './GenericConsentField';

export declare class GenericConsentFieldGroup {
    [k: string]: GenericConsentField;
    metadata: {
        name: string,
        id?: string,
        description?: string,
        archived?: boolean,
        url?: string,
        expired?: boolean,
    };
}

export declare interface GenericConsentFieldDocument {
    [k: string]: GenericConsentFieldGroup;
}