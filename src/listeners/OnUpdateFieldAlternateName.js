import { DataObjectState } from '@themost/data';
import { QueryExpression } from '@themost/query';
/**
 * 
 * @param {import('@themost/data').DataEventArgs} event 
 * @param {Function} callback 
 */
export function afterSave(event, callback) {
    if (event.state === DataObjectState.Update) {
        const target = event.target;
        const context = event.model.context;
        if (target.alternateName != null && event.previous != null && target.alternateName !== event.previous.alternateName) {
            try {
                const { sourceAdapter: entity } = context.model('UserConsent') ;
                // important: the operation will perform an update on all consent fields
                // for the specified group without using consent fields model
                const updateQuery = new QueryExpression().update(entity).set({
                    consent: target.alternateName
                }).where('consent').equal(event.previous.alternateName);
                // execute update query
                return context.db.executeAsync(updateQuery).then(() => {
                    return callback();
                }).catch((err) => {
                    return callback(err);
                });
            } catch (error) {
                return callback(error);
            }
        }
    }
    return callback();
}