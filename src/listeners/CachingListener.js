import {TextUtils, TraceUtils} from '@themost/common';
import {DataCacheStrategy} from '@themost/data';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeExecute(event, callback) {
    try {
        const {context} = event.model;
        if (event.query && event.query.$select) {
            // create query hash
            const hash = TextUtils.toMD5(event.query);
            // generate cache key
            const key = `/${event.model.name}/?query=${hash}&lang=${context.locale}`;
            return context.getConfiguration().getStrategy(DataCacheStrategy).get(key).then((result) => {
                if (typeof result !== 'undefined') {
                    if (event.emitter) {
                        delete event.emitter.$expand;
                    }
                    event.cached = true;
                    event.result = result;
                    return callback();
                }
                return callback();
            }).catch((err)=> {
                return callback(err);
            });
        }
        return callback();
    }
    catch (err) {
        return callback(err);
    }
}

export function afterExecute(event, callback) {
    try {
        const {context} = event.model;
        if (event.query && event.query.$select) {
            if (typeof event.result !== 'undefined' && !event.cached) {
                // create hash
                const hash = TextUtils.toMD5(event.query);
                // generate cache key
                const key = `/${event.model.name}/?query=${hash}&lang=${context.locale}`;
                context.getConfiguration().getStrategy(DataCacheStrategy).add(key, event.result);
                return callback();
            }
        }
        callback();
    }
    catch (err) {
        return callback(err);
    }
}

export function afterSave(event, callback) {
    try {
        const {context} = event.model;
        const service = context.getConfiguration().getStrategy(DataCacheStrategy);
        if (service) {
            return service.remove(`/${event.model.name}/*`).catch((err) => {
                TraceUtils.warn('An error occurred while removing cache keys');
                TraceUtils.warn(err);
            }).finally(() => {
                callback();
            });
        }
        callback();
    }
    catch (err) {
        return callback(err);
    }
}

export function afterRemove(event, callback) {
    try {
        const {context} = event.model;
        const service = context.getConfiguration().getStrategy(DataCacheStrategy);
        if (service) {
            return service.remove(`/${event.model.name}/*`).catch((err) => {
                TraceUtils.warn('An error occurred while removing cache keys');
                TraceUtils.warn(err);
            }).finally(() => {
                callback();
            });
        }
        callback();
    }
    catch (err) {
        return callback(err);
    }
}
